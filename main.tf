/*Crear una una subred con nombre a partir de una nueva VPC con nombre*/

provider "aws" {
  region     = "us-east-1"
}

resource "aws_vpc" "development-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name: "vpc-development"
    vpc_env: "dev"
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id     = aws_vpc.development-vpc.id
  cidr_block = "10.0.8.0/24"
  availability_zone = "us-east-1a"
    tags = {
    Name: "subnet-1-dev"
  }
}